#!/bin/bash -l

cd ./docker
docker pull localhost:5000/react-app:latest
docker pull localhost:5000/django-app:latest

if docker stack ls | grep "$2"; then
  if [[ "$2" -eq "dev" ]]; then
    echo "Updating dev stack"
  else
    echo "Updating prod stack"
  fi

  export STAGE=${2}_
  docker service update --image localhost:5000/react-app:latest ${STAGE}react;
  docker service update --image localhost:5000/django-app:latest ${STAGE}django;
else
  echo 'No services is up'
  echo 'Launching....'
  docker stack deploy --with-registry-auth --prune -c ./$1 $2;
fi
