#!/bin/bash -l

apt update && apt install -y ntp mc htop screen wget docker.io docker docker-compose build-essential python-dev libreadline-dev libbz2-dev libssl-dev libsqlite3-dev libxslt1-dev libxml2-dev git curl mysql-client libmysqlclient-dev python3-dev
systemctl enable docker
service ntp restart

if docker stack ls | grep 'services'; then
  echo 'Stack already exists!'
else
  echo 'No Stack is deployed now. Deploying new Stack....'
  docker pull registry:2.7
  docker pull portainer/portainer-ce
  docker pull joxit/docker-registry-ui:latest
  docker pull traefik:v2.9
  docker stack deploy --with-registry-auth -c ../docker/docker-compose.services.yml services;
fi

cd ../web-app-la/backend/
docker build -t django-app:latest .
docker image tag react-app:latest localhost:5000/django-app:latest
docker push localhost:5000/django-app:latest

cd ../frontend/

# change proxy for package-json (frontend-build)
sed -i -e "s/localhost/$BACKEND_SERVER/" package.json

docker build -t react-app:latest .
docker image tag react-app:latest localhost:5000/react-app:latest
docker push localhost:5000/react-app:latest
