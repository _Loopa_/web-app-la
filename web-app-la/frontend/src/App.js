import './App.css';
import Header from './components/Header.js';
import NotesListPage from "./pages/NotesListPage.js";
import {
    BrowserRouter as Router,
    Route,
    Routes
} from "react-router-dom";
import NotePage from "./pages/NotePage.js"

function App() {
  return (
    <Router>
        <div className="container dark">
            <div className="app">
                <Header />
                <Routes>
                    <Route path="/" element={<NotesListPage />} />
                    <Route path="/note/:id" element={<NotePage />} />
                </Routes>
            </div>
        </div>
    </Router>
  );
}

export default App;
